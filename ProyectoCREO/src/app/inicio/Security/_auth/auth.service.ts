import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtModel } from './jwt-model';
import { User } from 'src/app/users/model/user';
import { environment } from 'src/environments/environment';


const cabecera = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private httpClient: HttpClient) { }

  public login(usuario: User): Observable<JwtModel> {
    return this.httpClient.post<JwtModel>(`${environment.apiUrl}/auth/login`, usuario, cabecera);
  }

  public registro(usuario: User): Observable<any> {
    return this.httpClient.post<any>(`${environment.apiUrl}/auth/nuevo`, usuario, cabecera);
  }
}
