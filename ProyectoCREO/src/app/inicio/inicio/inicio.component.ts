import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  imagenPromocional;

  constructor() { }

  ngOnInit(): void {
    this.imagenPromocional = environment.imagenPromocional;

    //window.location.reload();

  }



}
