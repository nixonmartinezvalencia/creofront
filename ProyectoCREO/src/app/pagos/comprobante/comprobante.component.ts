import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-comprobante',
  templateUrl: './comprobante.component.html',
  styleUrls: ['./comprobante.component.css']
})
export class ComprobanteComponent implements OnInit {

  comprobantePago;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.comprobantePago = environment.imgComprobantePago;
  }

  goToPago() {

    this.router.navigate(['pagos', 'pago']);
  }



  descargarComprobante() {

  }


}
