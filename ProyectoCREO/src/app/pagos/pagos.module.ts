import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagosRoutingModule } from './pagos-routing.module';
import { PagoComponent } from './pago/pago.component';
import { ComprobanteComponent } from './comprobante/comprobante.component';
import { ResumenPagoComponent } from './resumen-pago/resumen-pago.component';
import { PseComponent } from './pse/pse.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [

    PagoComponent,
    ComprobanteComponent,
    ResumenPagoComponent,
    PseComponent],

  imports: [
    CommonModule,
    PagosRoutingModule,
    FormsModule
  ]
})
export class PagosModule { }
