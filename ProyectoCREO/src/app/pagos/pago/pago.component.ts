import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css']
})
export class PagoComponent implements OnInit {

  valorPago: number = 0;

  constructor(

    private router: Router,
  ) { }

  ngOnInit(): void {
  }


  pagar() {

    this.ocultar();

  }


  generarComprobante() {

    this.router.navigate(["pagos", "comprobante"]);

  }

  ingresarPse() {

    this.router.navigate(["pagos", "pse"]);
  }

  ocultar() {

    const demoId = document.querySelector('#formContent');
    demoId.setAttribute('style', 'display:none');

    const demoId2 = document.querySelector('#formContent2');
    demoId2.setAttribute('style', 'display:block');

  }

}
