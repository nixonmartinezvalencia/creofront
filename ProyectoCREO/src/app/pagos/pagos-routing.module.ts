import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagoComponent } from './pago/pago.component';
import { ComprobanteComponent } from './comprobante/comprobante.component';
import { ResumenPagoComponent } from './resumen-pago/resumen-pago.component';
import { PseComponent } from './pse/pse.component';

const routes: Routes = [


  {
    path: '',
    children: [

      {
        path: 'pago',
        component: PagoComponent,
      },

      {
        path: 'comprobante',
        component: ComprobanteComponent,
      }
      ,
      {
        path: 'resumentPago',
        component: ResumenPagoComponent,
      }
      ,
      {
        path: 'pse',
        component: PseComponent,
      }

    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagosRoutingModule { }
