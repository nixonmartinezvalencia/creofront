import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeudoresComponent } from './codeudores.component';

describe('CodeudoresComponent', () => {
  let component: CodeudoresComponent;
  let fixture: ComponentFixture<CodeudoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CodeudoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeudoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
