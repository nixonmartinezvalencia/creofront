import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/users/model/user';
import { UserService } from 'src/app/users/service/user.service';
import { WorkingInformation } from 'src/app/users/model/WorkingInformation';
import { Utility } from 'src/app/users/model/Utility';
import { environment } from 'src/environments/environment';

declare var jQuery: any;

@Component({
  selector: 'app-informacion-laboral',
  templateUrl: './informacion-laboral.component.html',
  styleUrls: ['./informacion-laboral.component.css']
})
export class InformacionLaboralComponent implements OnInit {

  @ViewChild('createModal', { static: false }) createModal: ElementRef;

  googleMaps1;

  numeroSolicitud: string = "";
  isUpdate: boolean = false;

  utility: Utility = new Utility();
  tipoTrabajadores: string[] = this.utility.tipoTrabajadores;
  lugaresDeTrabajo: string[] = this.utility.ciudades;
  tiposDeOficina: string[] = this.utility.oficinas;

  currentUser: string;
  user: User = new User();

  workingInformation: WorkingInformation = new WorkingInformation();

  errorMsg = '';
  isLogin = false;

  form: any = {};

  constructor(private tokenService: TokenService,
    private userService: UserService,
    private router: Router,
    private rutaActiva: ActivatedRoute) { }

  ngOnInit() {

    this.googleMaps1 = environment.imgGoogleMaps1;
    this.numeroSolicitud = this.rutaActiva.snapshot.paramMap.get("numeroSolicitud");

    if (this.numeroSolicitud) {
      this.isUpdate = true;
    }


    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;

          if (this.user.workingInformation !== null) {
            this.workingInformation = this.user.workingInformation;

          }

        });
    }
  }

  ocultar() {

    const demoId = document.querySelector('#formContent');
    demoId.setAttribute('style', 'display:none');

  }

  registrar() {

    this.userService.updateWorkingInformation(this.workingInformation, this.user.id).subscribe(data => {

      this.workingInformation = data;

    });

    if (this.numeroSolicitud != null) {
      this.router.navigate(['solicitudDeCredito', 'referencias', this.numeroSolicitud]);


    } else {

      this.router.navigate(['solicitudDeCredito', 'referencias']);
    }

  }

  logOut(): void {

    this.tokenService.logOut();
    this.router.navigate(['inicio']);

  }

  registrarDireccion() {
    this.workingInformation.direccionOficina = "calle " + this.form.calle + " " + " carrera " + this.form.carrera + " barrio " + this.form.barrio + " " + this.form.numeroVivienda;

    jQuery(this.createModal.nativeElement).modal('hide');


  }

}
