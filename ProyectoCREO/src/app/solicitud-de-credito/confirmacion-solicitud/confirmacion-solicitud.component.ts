import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { UserService } from 'src/app/users/service/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from 'src/app/users/model/user';
import { Credito } from 'src/app/users/model/Credito';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-confirmacion-solicitud',
  templateUrl: './confirmacion-solicitud.component.html',
  styleUrls: ['./confirmacion-solicitud.component.css']
})
export class ConfirmacionSolicitudComponent implements OnInit {

  id: string = "";
  currentUser: string;
  user: User = new User();

  credito: Credito = new Credito();

  errorMsg = '';
  isLogin = false;

  constructor(

    private tokenService: TokenService,
    private userService: UserService,
    private rutaActiva: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {

    this.id = this.rutaActiva.snapshot.paramMap.get("id");

    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;

    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {
          this.user = data;

        });
    }

  }

  solicitud() {
    this.router.navigate(['estadoSolicitud', 'solicitud', this.id]);
  }

  estadoSolicitud() {
    this.router.navigate(['estadoSolicitud', 'estado']);
  }

}
