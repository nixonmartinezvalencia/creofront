import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitudDeCreditoRoutingModule } from './solicitud-de-credito-routing.module';
import { CreditosComponent } from './creditos/creditos.component';
import { ConfirmacionSolicitudComponent } from './confirmacion-solicitud/confirmacion-solicitud.component';



@NgModule({
  declarations: [
    CreditosComponent,
    ConfirmacionSolicitudComponent,


  ],
  imports: [
    CommonModule,
    SolicitudDeCreditoRoutingModule
  ]
})
export class SolicitudDeCreditoModule { }
