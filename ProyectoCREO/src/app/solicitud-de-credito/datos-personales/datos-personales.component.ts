import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/users/model/user';
import { PersonalData } from 'src/app/users/model/PersonalData';
import { UserService } from 'src/app/users/service/user.service';
import { Utility } from 'src/app/users/model/Utility';
import { environment } from 'src/environments/environment';

declare var jQuery: any;

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.css']
})
export class DatosPersonalesComponent implements OnInit {

  @ViewChild('createModal', { static: false }) createModal: ElementRef;
  @ViewChild('formContent') formContent;

  googleMaps1;

  numeroSolicitud: string = "";
  isUpdate: boolean = false;

  utility: Utility = new Utility();
  ciudades: string[] = this.utility.ciudades;
  departamentos: string[] = this.utility.departamentos;
  viviendas: string[] = this.utility.viviendas;
  vehiculos: string[] = this.utility.vehiculos;
  cuentas: string[] = this.utility.cuentas;

  currentUser: string;
  user: User = new User();

  personalData: PersonalData = new PersonalData();

  errorMsg = '';
  isLogin = false;

  form: any = {};

  constructor(private tokenService: TokenService,
    private userService: UserService,
    private router: Router,
    private rutaActiva: ActivatedRoute) { }

  ngOnInit() {
    this.googleMaps1 = environment.imgGoogleMaps1;

    this.numeroSolicitud = this.rutaActiva.snapshot.paramMap.get("numeroSolicitud");


    if (this.numeroSolicitud) {
      this.isUpdate = true;
    }

    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;

    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;
          this.personalData = this.user.personalData;

        });
    }

  }

  ocultar() {

    const demoId = document.querySelector('#formContent');
    demoId.setAttribute('style', 'display:none');

    const demoId2 = document.querySelector('#formContent2');
    demoId2.setAttribute('style', 'display:block');

  }

  registrar() {
    this.userService.updatePersonalData(this.personalData, this.user.id).subscribe(data => {
      this.personalData = data;

    });

    this.ocultar();

  }

  registrar2() {

    this.userService.updatePersonalData(this.personalData, this.user.id).subscribe(data => {
      this.personalData = data;

    });
    if (this.numeroSolicitud != null) {
      this.router.navigate(['solicitudDeCredito', 'informacionLaboral', this.numeroSolicitud]);

    } else {

      this.router.navigate(['solicitudDeCredito', 'informacionLaboral']);
    }

  }

  logOut(): void {

    this.tokenService.logOut();
    this.router.navigate(['inicio']);

  }

  registrarDireccion() {
    this.personalData.direccion = "calle " + this.form.calle + " " + " carrera " + this.form.carrera + " barrio " + this.form.barrio + " " + this.form.numeroVivienda;

    jQuery(this.createModal.nativeElement).modal('hide');

  }

}