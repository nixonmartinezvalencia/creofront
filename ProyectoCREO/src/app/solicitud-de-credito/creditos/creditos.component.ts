import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/users/service/user.service';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { User } from 'src/app/users/model/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-creditos',
  templateUrl: './creditos.component.html',
  styleUrls: ['./creditos.component.css']
})
export class CreditosComponent implements OnInit {

  errorMsg = '';
  id: string = "";
  wrongForm = false;
  form: any = {};
  currentUser: string;
  isLogin = false;
  user: User = new User();

  constructor(


    private tokenService: TokenService,
    private userService: UserService,
    private router: Router,

  ) {

  }

  ngOnInit() {


    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;




        });
    }



  }

  enterPersonalData() {

    this.router.navigate(['solicitudDeCredito', 'datosPersonales']);
  }

}
