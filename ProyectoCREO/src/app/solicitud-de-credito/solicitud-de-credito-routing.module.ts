import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CodeudoresComponent } from './codeudores/codeudores.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';

import { ReferenciasComponent } from './referencias/referencias.component';
import { InformacionLaboralComponent } from './informacion-laboral/informacion-laboral.component';
import { CreditosComponent } from './creditos/creditos.component';
import { EntregaComponent } from './entrega/entrega.component';
import { ConfirmacionSolicitudComponent } from './confirmacion-solicitud/confirmacion-solicitud.component';


const routes: Routes = [


  {
    path: '',
    children: [

      {

        path: 'codeudores',
        component: CodeudoresComponent,
      },
      {

        path: 'codeudores/:numeroSolicitud',
        component: CodeudoresComponent,
      },
      {

        path: 'entrega',
        component: EntregaComponent,
      },
      {

        path: 'entrega/:numeroSolicitud',
        component: EntregaComponent,
      },
      {

        path: 'datosPersonales',
        component: DatosPersonalesComponent,
      },
      {

        path: 'datosPersonales/:numeroSolicitud',
        component: DatosPersonalesComponent,
      },
      {

        path: 'informacionLaboral',
        component: InformacionLaboralComponent,
      },
      {

        path: 'informacionLaboral/:numeroSolicitud',
        component: InformacionLaboralComponent,
      },
      {

        path: 'referencias',
        component: ReferenciasComponent,
      },
      {

        path: 'referencias/:numeroSolicitud',
        component: ReferenciasComponent,
      },
      {

        path: 'creditos',
        component: CreditosComponent,
      },
      {

        path: 'confirmacion',
        component: ConfirmacionSolicitudComponent,
      },
      {

        path: 'confirmacion/:id',
        component: ConfirmacionSolicitudComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitudDeCreditoRoutingModule { }
