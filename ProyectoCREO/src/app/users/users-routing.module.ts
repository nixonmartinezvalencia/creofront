import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { CredencialesComponent } from './credenciales/credenciales.component';
import { AutenticacionComponent } from './autenticacion/autenticacion.component';

const routes: Routes = [
{
    path: '' ,  
    children:[

      {

        path:'datosPersonales',
        component :DatosPersonalesComponent,
      },

      {

        path:'datosPersonales/:id',
        component :DatosPersonalesComponent,
      },
      {

        path:'credenciales/:id',
        component :CredencialesComponent,
      },
      {

        path:'credenciales/:id/:confirmacion',
        component :CredencialesComponent,
      },
      {

        path:'autenticacion',
        component :AutenticacionComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
