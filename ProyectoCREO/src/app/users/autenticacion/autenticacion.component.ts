import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { PersonalData } from '../model/PersonalData';
import { UserService } from 'src/app/users/service/user.service';
import { Validation } from '../model/Validation';
import { Router } from '@angular/router';
import { Utility } from '../model/Utility';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-autenticacion',
  templateUrl: './autenticacion.component.html',
  styleUrls: ['./autenticacion.component.css']
})
export class AutenticacionComponent implements OnInit {

  ImagenAutenticacion;

  utility: Utility = new Utility();
  tiposDeDocumento: string[] = this.utility.tiposDeDocumento;

  user: User = new User();
  personalData: PersonalData = new PersonalData();
  validation: Validation = new Validation();
  validationForm: Validation = new Validation();

  codigo: string = "";

  codigos: string[] = [];

  errorMsg = "";

  constructor(
    private userService: UserService, private router: Router
  ) { }

  ngOnInit(): void {

    this.ImagenAutenticacion = environment.imImagenAutenticacion;
    this.codigos = ["", "", "", "", "", ""];

    this.personalData.tipoIdentificacion = this.tiposDeDocumento[0];
  }

  validarUsuario() {
    this.userService.detailByIdentificacion(this.personalData.identificacion).subscribe(data => {

      this.validation = data;
      if (this.validation.mensaje == 'debes registrarte') {
        this.router.navigate(['users', 'datosPersonales']);
      }

    });



  }

  validData(data: Validation, mensaje: string) {

    if (data.codigoVerificacion == '') {

      this.errorMsg = mensaje;
    } else {

      this.validation = data;
      this.errorMsg = "";

    }

  }

  validNumberPhone() {


    this.userService.detailByIdAndPhone(this.validation.user.personalData.identificacion, this.personalData.telefonoFijo).subscribe(data => {

      this.validData(data, "Numero incorrecto");


    });



  }

  validEmail() {

    this.userService.detailByIdAndEmail(this.validation.user.personalData.identificacion, this.personalData.correo).subscribe(data => {


      this.validData(data, "Email incorrecto");

    });

  }

  validarCodigo() {

    this.codigo = "";

    this.codigos.forEach(element => {

      this.codigo += element;

    });



    if (this.codigo == this.validation.codigoVerificacion) {

      this.router.navigate(['users', 'datosPersonales', this.validation.user.id]);
    }

    else {

      this.errorMsg = "token incorrecto";

    }

  }



}
