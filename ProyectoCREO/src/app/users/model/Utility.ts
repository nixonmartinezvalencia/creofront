

export class Utility {


    cuentas: string[] = ['Ahorros', 'Corriente'];
    ciudades: string[] =
        [
            'Arauca',
            'Armenia',
            'Barranquilla',
            'Bogotá',
            'Bucaramanga',
            'Cali',
            'Cartagena',
            'Cúcuta',
            'Florencia',
            'Ibagué',
            'Inírida',
            'Leticia',
            'Manizales',
            'Medellín',
            'Mitú',
            'Mocoa',
            'Montería',
            'Neiva',
            'Pasto',
            'Pereira',
            'Popayán',
            'Puerto Carreño',
            'Quibdó',
            'Riohacha',
            'San Andrés',
            'San José del Guaviare',
            'Santa Marta',
            'Sincelejo',
            'Tunja',
            'Valledupar',
            'Villavicencio',
            'Yopal'

        ];

    departamentos: string[] =
        [
            'Amazonas',
            'Antioquia',
            'Arauca',
            'Atlántico',
            'Bolívar',
            'Boyacá',
            'Caldas',
            'Caquetá',
            'Casanare',
            'Cauca',
            'Cesar',
            'Chocó',
            'Córdoba',
            'Cundinamarca',
            'Guainía',
            'Guaviare',
            'Huila',
            'La Guajira',
            'Magdalena',
            'Meta',
            'Nariño',
            'Norte de Santander',
            'Putumayo',
            'Quindío',
            'Risaralda',
            'San Andrés y Providencia',
            'Santander',
            'Sucre',
            'Tolima',
            'Valle del Cauca',
            'Vaupés',
            'Vichada'

        ];

    viviendas: string[] =
        [
            'arrendada',
            'familiar',
            'propia'

        ];

    oficinas: string[] =
        [
            'apartaestudio',
            'independiente',
            'propia'

        ];

    vehiculos: string[] =
        [
            'motocicleta',
            'motocarro',
            'mototriciclo',
            'cuatrimoto',
            'campero',
            'camioneta',
            'microbús',
            'bus',
            'buseta',
            'camión',
            'tractocamión',
            'volqueta',
            'buggy',
            'convertible',
            'coupe',
            'hatchback',
            'sedán',
            'limosina'


        ];

    tipoTrabajadores: string[] = ['dependiente', 'independiente'];
    tiposDeDocumento: string[] = ['Cédula de ciudadania', 'Cédula de extranjeria', 'pasaporte'];
    tiposDeReferencia: string[] = ['familiar', 'laboral', 'personal'];


}