

export class WorkingInformation {

    tipoTrabajador: string = "";
    ingresos: string = "";
    lugarTrabajo: string = "";
    otrosIngresos: string = "";
    empresa: string = "";
    telefonoEmpresa: string = "";
    direccionOficina: string = "";
    tipoOficina: string = "";
    telefonoOficina: string = "";

}