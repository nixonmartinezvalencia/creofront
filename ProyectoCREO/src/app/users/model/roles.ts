import { Permission } from './permission';

export class Role {
    id?: string="";
    roleName: string="";
    enable: boolean=true;
    permissions: Permission[];
}