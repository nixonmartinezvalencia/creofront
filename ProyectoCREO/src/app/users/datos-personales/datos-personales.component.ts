import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { User } from '../model/user';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { UserService } from '../service/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonalData } from '../model/PersonalData';
import { Role } from '../model/roles';
import { Permission } from '../model/permission';
import { Utility } from '../model/Utility';
import { environment } from 'src/environments/environment';
import { Validation } from '../model/Validation';

declare var jQuery: any;

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.css']
})
export class DatosPersonalesComponent implements OnInit {

  @ViewChild('createModal', { static: false }) createModal: ElementRef;

  utility: Utility = new Utility();
  viviendas: string[] = this.utility.viviendas;

  id: string = "";
  user: User = new User();
  rol: Role = new Role();

  personalData: PersonalData = new PersonalData();

  errorMsg = '';
  isErrorUser = false;

  form: any = {};

  validation: Validation = new Validation();

  constructor(private tokenService: TokenService,
    private userService: UserService,
    private router: Router,
    private rutaActiva: ActivatedRoute) { }

  ngOnInit() {

    this.id = this.rutaActiva.snapshot.paramMap.get("id");

    if (this.id !== null) {
      this.userService.detail(this.id).subscribe(data => {
        this.user = data;
        this.personalData = this.user.personalData;

      },
        err => {
          console.log(err);
        }
      );

    }
  }

  registrar() {

    this.rol.roleName = "Admin General";
    this.rol.enable = true;
    this.rol.permissions = [new Permission("Editor")];

    this.user.rol = this.rol;

    this.user.username = "usuarioPrueba";

    this.user.password = environment.credencialAcceso;

    this.user.personalData = this.personalData;

    this.userService.detailByIdentificacion(this.personalData.identificacion).subscribe(data => {

      this.validation = data;

      if (this.validation.mensaje == 'debes registrarte' || this.id != null) {

        this.userService.create(this.user).subscribe(data => {

          this.user = data;

          this.router.navigate(['users', 'credenciales', this.user.id]);

        });


      } else {
        this.isErrorUser = true;
        this.errorMsg = "El numero de identificacion ya se encuentra registrado";

      }

    });

  }

  logOut(): void {

    this.tokenService.logOut();
    this.router.navigate(['inicio']);

  }

  registrarDireccion() {
    this.personalData.direccion = "calle " + this.form.calle + " " + " carrera " + this.form.carrera + " barrio " + this.form.barrio + " " + this.form.numeroVivienda;

    jQuery(this.createModal.nativeElement).modal('hide');

  }
}