import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SolicitudComponent } from './solicitud/solicitud.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { EstadoComponent } from './estado/estado.component';
import { GenerarPagareComponent } from './generar-pagare/generar-pagare.component';


const routes: Routes = [


  {
    path: '',
    children: [

      {

        path: 'solicitud',
        component: SolicitudComponent,
      },
      {

        path: 'solicitud/:id',
        component: SolicitudComponent,
      },
      {
        path: 'solicitudes',
        component: SolicitudesComponent,
      }
      ,
      {
        path: 'estado',
        component: EstadoComponent,
      },

      {
        path: 'generarPagare',
        component: GenerarPagareComponent,
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstadoSolicitudRoutingModule { }
