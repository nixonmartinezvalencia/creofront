import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstadoSolicitudRoutingModule } from './estado-solicitud-routing.module';
import { CreditoComponent } from './credito/credito.component';
import { SolicitudComponent } from './solicitud/solicitud.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { EstadoComponent } from './estado/estado.component';
import { GenerarPagareComponent } from './generar-pagare/generar-pagare.component';


@NgModule({
  declarations: [CreditoComponent, SolicitudComponent, SolicitudesComponent, EstadoComponent, GenerarPagareComponent],
  imports: [
    CommonModule,
    EstadoSolicitudRoutingModule
  ]
})
export class EstadoSolicitudModule { }
