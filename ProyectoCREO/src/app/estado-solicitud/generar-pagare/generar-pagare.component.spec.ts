import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarPagareComponent } from './generar-pagare.component';

describe('GenerarPagareComponent', () => {
  let component: GenerarPagareComponent;
  let fixture: ComponentFixture<GenerarPagareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerarPagareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarPagareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
