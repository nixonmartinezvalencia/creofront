import { Component, OnInit } from '@angular/core';
import { CreditoService } from 'src/app/users/service/Credito.service';
import { SolicitudCredito } from 'src/app/users/model/SolicitudCredito';
import { User } from 'src/app/users/model/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { UserService } from 'src/app/users/service/user.service';
import { SolicitudCreditoService } from 'src/app/users/service/SolicitudCredito.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-generar-pagare',
  templateUrl: './generar-pagare.component.html',
  styleUrls: ['./generar-pagare.component.css']
})
export class GenerarPagareComponent implements OnInit {

  imgVistaPreviaPagare;
  currentUser: string;
  isLogin = false;
  id: string = "";
  user: User = new User();
  solicitudesCredito: SolicitudCredito[] = [];
  solicitudCredito: SolicitudCredito = new SolicitudCredito();

  constructor(
    private tokenService: TokenService,
    private creditoService: CreditoService,
    private userService: UserService,
    private solicitudCreditoService: SolicitudCreditoService,
    private router: Router,
  ) { }

  ngOnInit() {

    // this.id = this.rutaActiva.snapshot.paramMap.get("id");
    this.imgVistaPreviaPagare =environment.imgVistaPreviaPagare;
    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();

    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;


    }

    if (this.isLogin) {

      this.userService.detailByName(this.tokenService.getUserName())
        .subscribe(data => {

          this.user = data;

          this.solicitudCreditoService.getSolicitudesByUsuario(this.user.id).subscribe(data2 => {

            if (data2 != null) {

              this.solicitudesCredito = data2;

              this.solicitudCredito = this.solicitudesCredito[this.solicitudesCredito.length - 1];

            }



          });



        });
    }


  }



  generateReportPDF() {

    let mensaje = "";

    this.creditoService.generateReportPDF(this.user.id, this.solicitudCredito.numeroSolicitud).subscribe(data => {

      mensaje = data;

    });



  }

  goToEstado(){

    this.router.navigate(['estadoSolicitud', 'estado']);
  }

}
