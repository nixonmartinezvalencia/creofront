export const environment = {

  production: true,
  credencialAcceso: "Cc1234567",



  imgLogo: '/CREO/assets/img/logo.bmp',
  imgcheck: '/CREO/assets/img/check.png',
  imgAprobacionDirector: '/CREO/assets/img/aprobacionDirector.bmp',
  imgCheckError: '/CREO/assets/img/checkError.jpg',
  imgComprobantePago: '/CREO/assets/img/comprobantePago.jpg',
  imgEntregaDinero: '/CREO/assets/img/entregaDinero.bmp',
  imgEstadoSolicitud: '/CREO/assets/img/estadoSolicitud.png',
  imgGoogleMaps1: '/CREO/assets/img/googleMaps1.jpg',
  imGoogleMaps2: '/CREO/assets/img/googleMaps2.jpg',
  imgIconfacebook: '/CREO/assets/img/Iconfacebook.png',
  imgIconInstagram: '/CREO/assets/img/IconInstagram.png',
  imgIconTwiter: '/CREO/assets/img/iconTwiter.png',
  imgIconYouTube: '/CREO/assets/img/iconYouTube.png',
  imagenPromocional: '/CREO/assets/img/imagenPromocional.bmp',
  imImagenAutenticacion: '/CREO/assets/img/ImagenAutenticacion.jpg',
  imgPse: '/CREO/assets/img/imgpse.png',
  imgLogo2: '/CREO/assets/img/logo2.png',
  imgPagar: '/CREO/assets/img/pagar.png',
  imgSesion: '/CREO/assets/img/sesion.bmp',
  imgSolicitarCredito: '/CREO/assets/img/solicitarCredito.png',
  imgSolicitudRadicada: '/CREO/assets/img/solicitudRadicada.bmp',
  imgValidacionInformacion: '/CREO/assets/img/validacionInformacion.bmp',
  imgVistaPreviaPagare: '/CREO/assets/img/vistaPreviaPagare.png',
  imgLinkDescarga: '/CREO/assets/img/linkDescarga.png',




  apiUrl: 'http://191.235.87.18/:15020/apiCitas/api',
  Urllocalhost: 'http://191.235.87.18/:15020/apiCitas',
};
