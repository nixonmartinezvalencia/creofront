
export const environment = {
  production: false,

  imgLogo: '../../../assets/img/logo.bmp',
  imgcheck: '../../../assets/img/check.png',
  imgAprobacionDirector: '../../../assets/img/aprobacionDirector.bmp',
  imgCheckError: '../../../assets/img/checkError.jpg',
  imgComprobantePago: '../../../assets/img/comprobantePago.jpg',
  imgEntregaDinero: '../../../assets/img/entregaDinero.bmp',
  imgEstadoSolicitud: '../../../assets/img/estadoSolicitud.png',
  imgGoogleMaps1: '../../../assets/img/googleMaps1.jpg',
  imGoogleMaps2: '../../../assets/img/googleMaps2.jpg',
  imgIconfacebook: '../../../assets/img/Iconfacebook.png',
  imgIconInstagram: '../../../assets/img/IconInstagram.png',
  imgIconTwiter: '../../../assets/img/iconTwiter.png',
  imgIconYouTube: '../../../assets/img/iconYouTube.png',
  imagenPromocional: '../../../assets/img/imagenPromocional.bmp',
  imImagenAutenticacion: '../../../assets/img/ImagenAutenticacion.jpg',
  imgPse: '../../../assets/img/imgpse.png',
  imgLogo2: '../../../assets/img/logo2.png',
  imgPagar: '../../../assets/img/pagar.png',
  imgSesion: '../../../assets/img/sesion.bmp',
  imgSolicitarCredito: '../../../assets/img/solicitarCredito.png',
  imgSolicitudRadicada: '../../../assets/img/solicitudRadicada.bmp',
  imgValidacionInformacion: '../../../assets/img/validacionInformacion.bmp',
  imgVistaPreviaPagare: '../../../assets/img/vistaPreviaPagare.png',
  imgLinkDescarga: '../../../assets/img/linkDescarga.png',


  credencialAcceso: "Cc1234567",
  apiUrl: 'http://191.235.87.18:22020/apiCitas/api',
  Urllocalhost: 'http://191.235.87.18:22020/apiCitas',
 // crossOrigin: 'http://191.235.87.18:25020',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
  // import 'zone.js/dist/zone-error';  // Included with Angular CLI.
